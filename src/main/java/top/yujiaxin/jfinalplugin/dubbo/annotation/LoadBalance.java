package top.yujiaxin.jfinalplugin.dubbo.annotation;

public enum LoadBalance {
	RANDOM,ROUNDROBIN ,LEASTACTIVE ,CONSISTENTHASH ;
}
